import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {useState, useEffect} from 'react';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import Ent from 'react-native-vector-icons/Entypo';

const Signup = props => {
  const [cont, setCont] = useState('');
  const [show, setShow] = useState(false);
  const [confirm, setConfirm] = useState('');
  const [name, setFullname] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');

  useEffect(() => {
    const timeout = setTimeout(() => {
      setShow(!show);
    }, 5000);
    return () => clearTimeout(timeout);
  }, ['']);

  return (
    <ImageBackground
      source={require('../assets/images/aqua.jpg')}
      style={styles.container}>
      <View style={styles.headertext}>
        <Text style={styles.headertext1}>Create Account</Text>
        <Text style={styles.headertext2}>Please fill in the input below</Text>
      </View>
      <View style={styles.form}>
        <View style={{marginBottom: 30}}>
          <FloatingLabelInput
            label={'Fullname'}
            value={name}
            // hintTextColor={'grey'}
            // hint="Fullname"
            onChangeText={value => setFullname(value)}
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              borderColor: 'black',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'grey',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'black',
              paddingHorizontal: 5,
            }}
          />
        </View>
        <View style={{marginBottom: 30}}>
          <FloatingLabelInput
            label="Phone"
            value={phone}
            hintTextColor={'grey'}
            keyboardType="numeric"
            hint="+62 (81) 98765-4321"
            onChangeText={value => {
              setPhone(value);
            }}
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              borderColor: 'black',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'grey',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'black',
              paddingHorizontal: 5,
            }}
          />
        </View>
        <View style={{marginBottom: 30}}>
          <FloatingLabelInput
            label={'Email'}
            value={email}
            hintTextColor={'grey'}
            hint="Example@gmail.com"
            onChangeText={value => setEmail(value)}
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              borderColor: 'black',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'grey',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'black',
              paddingHorizontal: 5,
            }}
          />
        </View>
        <View style={{marginBottom: 30}}>
          <FloatingLabelInput
            label={'Password'}
            isPassword
            togglePassword={show}
            hintTextColor={'grey'}
            value={cont}
            onChangeText={value => setCont(value)}
            customShowPasswordComponent={
              <Ent name="eye" size={20} color={'black'} />
            }
            customHidePasswordComponent={
              <Ent name="eye-with-line" size={20} color={'black'} />
            }
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              borderColor: 'black',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'grey',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'black',
              paddingHorizontal: 5,
            }}
          />
        </View>
        <View style={{marginBottom: 50}}>
          <FloatingLabelInput
            label={'Confirm Password'}
            isPassword
            togglePassword={show}
            hintTextColor={'grey'}
            value={confirm}
            onChangeText={value => setConfirm(value)}
            customShowPasswordComponent={
              <Ent name="eye" size={20} color={'black'} />
            }
            customHidePasswordComponent={
              <Ent name="eye-with-line" size={20} color={'black'} />
            }
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              borderColor: 'black',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'grey',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'black',
              paddingHorizontal: 5,
            }}
          />
        </View>
      </View>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => props.navigation.navigate('Bottom')}>
        <Text style={styles.btntext}>Sign Up</Text>
      </TouchableOpacity>
      <View style={styles.btn2}>
        <Text style={styles.txtbtn}>Already have an Account ?</Text>
        <Text style={styles.txtbtn2}>Sign In</Text>
      </View>
    </ImageBackground>
  );
};

export default Signup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: `#191970`,
  },
  headertext: {
    marginBottom: 70,
    paddingRight: 79,
  },
  headertext1: {
    fontSize: 34,
    fontWeight: 'bold',
    color: `#000`,
  },
  headertext2: {
    fontSize: 15,
    fontWeight: 'bold',
    color: `#000`,
  },
  form: {
    width: '100%',
    marginBottom: 20,
  },
  txtInput: {
    borderColor: `#00bfff`,
    borderWidth: 1,
    marginBottom: 30,
    borderRadius: 10,
    color: 'white',
  },
  btn: {
    backgroundColor: `#000`,
    paddingVertical: 10,
    width: '50%',
    borderRadius: 100,
    alignItems: 'center',
    marginBottom: 30,
  },
  btntext: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  },
  btn2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  txtbtn: {
    color: 'white',
  },
  txtbtn2: {
    textDecorationLine: 'underline',
    color: `black`,
    marginLeft: 5,
  },
});
