import {View, Text} from 'react-native';
import React from 'react';
import Login from './src/Screen/Login/Login';

const App = () => {
  return (
    <View>
      <Login />
    </View>
  );
};

export default App;
